<?php

declare(strict_types=1);

namespace App\Task3;

use App\Task1\FightArena;
use App\Task1\Fighter;

class FightArenaHtmlPresenter
{
    public function present(FightArena $arena): string
    {
        $list = array_map(function ($fighter) {
            /**
             * @var Fighter $fighter
             */
            return sprintf('<div class="fighter">
                                        <div class="info">
                                            <p>%s: %d, %d</p>
                                        </div>
                                        <div class="image">
                                            <img src="%s">
                                        </div>
                                   </div>',
                $fighter->getName(),
                $fighter->getHealth(),
                $fighter->getAttack(),
                $fighter->getImage()
            );

        }, $arena->all());

        $content = implode("", $list);

        return $content;
    }
}
