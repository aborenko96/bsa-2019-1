<?php
/**
 * Created by PhpStorm.
 * User: Andrey
 * Date: 17.05.2019
 * Time: 15:42
 */

namespace App\Task1;


interface FighterInterface
{
    function getId(): int;

    function getName(): string;

    function getHealth(): int;

    function getAttack(): int;

    function getImage(): string;
}