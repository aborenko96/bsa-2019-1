<?php

declare(strict_types=1);

namespace App\Task1;

class FightArena
{
    /**
     * @property Fighter[] $fighters
     */
    private $fighters;

    public function add(Fighter $fighter): void
    {
        $this->fighters[] = $fighter;
    }

    public function mostPowerful(): Fighter
    {
        usort($this->fighters, function($firstFighter, $secondFighter){
            /**
             * @var Fighter $firstFighter
             * @var Fighter $secondFighter
             */
            return $secondFighter->getAttack() <=> $firstFighter->getAttack();
        });

        return reset($this->fighters);
    }

    public function mostHealthy(): Fighter
    {
        usort($this->fighters, function($firstFighter, $secondFighter){
            /**
             * @var Fighter $firstFighter
             * @var Fighter $secondFighter
             */
            return $secondFighter->getHealth() <=> $firstFighter->getHealth();
        });

        return reset($this->fighters);
    }

    public function all(): array
    {
        return $this->fighters;
    }
}
